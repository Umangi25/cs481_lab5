import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'dart:math';

void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.info_outline)),
                Tab(icon: Icon(Icons.list)),
                Tab(icon: Icon(Icons.add_comment)),
              ],
            ),
            title: Text('Techy Shopping Zone'),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.card_travel,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          body: TabBarView(
            children: [
              TooltipPage(),
              MyHomePage(),
              RatingPage(),
            ],
          ),
        ),

        /*TabBarView(
            children: [

              Icon(Icons.info_outline),

            ],
          ),*/
      ),
    );
    //);
  }
}

class TooltipPage extends StatefulWidget {
  @override
  _TooltipPageState createState() => _TooltipPageState();
}

class _TooltipPageState extends State<TooltipPage> {
  final List<String> _listItem = [
    'Iphone11',
    'Macbook Pro',
    'Samsung S20',
    'Insta CAM',
    'Airpods',
    'Apple Watch',
    'Alexa',
    'Beat headphones',
  ];

  final List<Color> myColors = [
    Colors.black87,
    Colors.indigoAccent,
    Colors.yellow,
    Colors.lightGreenAccent,
    Colors.pinkAccent,
    Colors.orange,
    Colors.teal,
    Colors.brown,
    Colors.blue,
    Colors.red
  ];

  var images = [
    "https://i.insider.com/5d77febe2e22af0d943d8e34?width=1100&format=jpeg&auto=webp",
    "https://rentyourmac.com/wp-content/uploads/2018/11/macbook-pro-13-retina-a.jpg",
    "https://images-na.ssl-images-amazon.com/images/I/71s5gL6GAXL._AC_SX425_.jpg",
    "https://cdn.thewirecutter.com/wp-content/uploads/2018/10/instant-cameras-2018-lowres-8477.jpg",
    "https://cdn.shopify.com/s/files/1/1264/9135/products/L_AP_HXP_GP_2000x.jpg?v=1571439631",
    "https://images-na.ssl-images-amazon.com/images/I/71fwbMm1NBL._AC_SL1500_.jpg",
    "https://static.bhphoto.com/images/multiple_images/images2500x2500/1572984044_IMG_1271497.jpg",
    "https://cnet3.cbsistatic.com/img/EH3Qy_pbj4EmrwZV-49BFHGOPT0=/1200x675/2019/10/14/72f0d57c-5237-45b7-9931-a5406e6fe9c7/beats-solo-pro-33.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0)),
        new Text("Welcome to Techy Shopping Zone!!!",textAlign:TextAlign.center ,style: TextStyle(fontSize: 25,color: Colors.teal),),
        Padding(padding: EdgeInsets.fromLTRB(0, 40, 0, 0)),
        new Container(
            height: 250,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: _listItem.length,
              itemBuilder: (BuildContext context, int index) => Card(
                color: Colors.transparent,
                elevation: 0,
                child: Container(
                    decoration: BoxDecoration(
                    color: myColors[Random().nextInt(6)],
                    borderRadius: BorderRadius.circular(10)),
                    width: 250,
                    child: Tooltip(
                        waitDuration: Duration.zero,
                        verticalOffset: 135,
                        padding: EdgeInsets.all(15),
                        message: _listItem[index],
                        child: Image.network(images[index]),
                ),
              )),
        ),
      ),
        Padding(padding: EdgeInsets.fromLTRB(0, 40, 0, 0)),
        new Text("Long press on image to see the product name.",textAlign:TextAlign.center ,style: TextStyle(fontSize: 20),),
    ],
    );
  }
}

class RatingPage extends StatefulWidget {
  @override
  _RatingPageState createState() => _RatingPageState();
}

class _RatingPageState extends State<RatingPage> {
  double rating;
  final List<String> _listItem = [
    'Iphone11',
    'Macbook Pro',
    'Samsung S20',
    'Insta CAM',
    'Airpods',
    'Apple Watch',
    'Alexa',
    'Beat headphones',
  ];

  var images = [
    "https://i.insider.com/5d77febe2e22af0d943d8e34?width=1100&format=jpeg&auto=webp",
    "https://rentyourmac.com/wp-content/uploads/2018/11/macbook-pro-13-retina-a.jpg",
    "https://images-na.ssl-images-amazon.com/images/I/71s5gL6GAXL._AC_SX425_.jpg",
    "https://cdn.thewirecutter.com/wp-content/uploads/2018/10/instant-cameras-2018-lowres-8477.jpg",
    "https://cdn.shopify.com/s/files/1/1264/9135/products/L_AP_HXP_GP_2000x.jpg?v=1571439631",
    "https://images-na.ssl-images-amazon.com/images/I/71fwbMm1NBL._AC_SL1500_.jpg",
    "https://static.bhphoto.com/images/multiple_images/images2500x2500/1572984044_IMG_1271497.jpg",
    "https://cnet3.cbsistatic.com/img/EH3Qy_pbj4EmrwZV-49BFHGOPT0=/1200x675/2019/10/14/72f0d57c-5237-45b7-9931-a5406e6fe9c7/beats-solo-pro-33.jpg",
  ];

  Widget buildMyRating(double init) {
    if (init < 1) init = 1.0;
    return RatingBar(
        initialRating: init,
        minRating: 1.0,
        direction: Axis.horizontal,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.purple,
            ),
        itemSize: 25.0,
        onRatingUpdate: (rating) {
          print(rating);
        });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      //itemCount: records.length,
      itemCount: _listItem.length,
      itemBuilder: (context, index) {
        return ListTile(
            leading: Image.network(
              images[index],
              fit: BoxFit.cover,
              height: 50,
              width: 50,
            ),
            title: Text(_listItem[index]),
            trailing: buildMyRating(Random().nextInt(5).toDouble()));
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //var records = List<String>.generate(20, (index) => "Record : $index");
  final List<String> _listItem = [
    'Iphone11',
    'Macbook Pro',
    'Samsung S20',
    'Insta CAM',
    'Airpods',
    'Apple Watch',
    'Alexa',
    'Beat headphones',
  ];

  var images = [
    "https://i.insider.com/5d77febe2e22af0d943d8e34?width=1100&format=jpeg&auto=webp",
    "https://rentyourmac.com/wp-content/uploads/2018/11/macbook-pro-13-retina-a.jpg",
    "https://images-na.ssl-images-amazon.com/images/I/71s5gL6GAXL._AC_SX425_.jpg",
    "https://cdn.thewirecutter.com/wp-content/uploads/2018/10/instant-cameras-2018-lowres-8477.jpg",
    "https://cdn.shopify.com/s/files/1/1264/9135/products/L_AP_HXP_GP_2000x.jpg?v=1571439631",
    "https://images-na.ssl-images-amazon.com/images/I/71fwbMm1NBL._AC_SL1500_.jpg",
    "https://static.bhphoto.com/images/multiple_images/images2500x2500/1572984044_IMG_1271497.jpg",
    "https://cnet3.cbsistatic.com/img/EH3Qy_pbj4EmrwZV-49BFHGOPT0=/1200x675/2019/10/14/72f0d57c-5237-45b7-9931-a5406e6fe9c7/beats-solo-pro-33.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      //itemCount: records.length,
      itemCount: _listItem.length,
      itemBuilder: (context, index) {
        return ListTile(
          leading: Image.network(
            images[index],
            fit: BoxFit.cover,
            height: 50,
            width: 50,
          ),
          title: Text(_listItem[index]),
          trailing: Container(
            width: 40,
            child: FlatButton(
              child: Icon(
                Icons.shopping_cart,
                color: Colors.purple,
              ),
              onPressed: () {
                showSnackBar(context, index);
              },
            ),
          ),
        );
      },
    );
  }

  void showSnackBar(BuildContext context, int index) {
    var addedRecord = _listItem[index];
    SnackBar snackBar = SnackBar(
      content: Text('$addedRecord Added to cart'),
      duration: Duration(seconds: 1),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
